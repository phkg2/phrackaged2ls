#!/bin/env sh
#

# Helper to die nicely.
die() {
	printf -- '\nError:'
	printf -- '\n%s\n' "$*"
	exit 1
}


# Transform name to have phrag- prefix
_phkg2_name_with_phrag() {
	myfull="${1%/}"
	myppath="${1%/*}"

	if [ "x${myppath}" == "x${myfull}" ] ; then
		myppath=""
	fi

	myname="${myfull##*/}"
	if [ "x${myname#phrag-}" == "x${myname}" ] ; then
		myname="phrag-${myname}"
	fi

	printf -- "${myppath:+${myppath}/}${myname}"
}



# Usage: phkg2_phrag_clone new-phrag [template-phrag]
# Precondition: CWD in TLD where new phrag will be cloned.
phkg2_phrag_clone() {
	if [ $# -eq 0 ] ; then
		printf -- '\nUsage: %s clone <new phrag> [template phrag]\n' "$(basename "$0" )"
		printf -- '\tUses template "%s" by default.\n' "$(_phkg2_name_with_phrag "${PHRAG_TEMPLATE_DEFAULT}")"
		return 1
	fi
	NEWPH="$(_phkg2_name_with_phrag "${1}")"
	NEWNAME="${NEWPH##*/}"
	TEMPPH="$(_phkg2_name_with_phrag "${2:-${PHRAG_TEMPLATE_DEFAULT}}")"
	TEMPNAME="${TEMPPH##*/}"

	[ -d "${TEMPPH}" ] || die "Can not find template phrag repo at '${TEMPPH}'."
	[ -d "${TEMPPH}/.git/" ] || die "'${TEMPPH}' does not look like a git repository."

	[ -d "${NEWPH}" ] && die "Directory for '${NEWPH}' already exists."

	git clone "${TEMPPH}" "${NEWPH}" || die "Could not clone phrag '${TEMPPH}' to '${NEWPH}'."

	cd "${NEWPH}"
		printf -- '\nClearing old remote origin.\n'
		git remote remove origin
		printf -- '\nUpdating README: "%s" -> "%s".\n' "${TEMPNAME}" "${NEWNAME}"
		sed -e 's/'"${TEMPNAME}"'/'"${NEWNAME}"'/g' \
			-i ./README
		printf -- '\nNote: README not yet committed in git.\nPlease add some flesh to the skeleton!\b'
	cd -

	printf -- '\n'"'%s' ready to work on!"'\n' "${NEWNAME}"
	printf -- '\n'"Don't forget to git add and git commit your work when you're done!"'\n'
	printf -- '\nTIP: When using gitlab, you can use a command similar to the following to push a new repo:\n'
	printf -- '\tgit push --set-upstream git@gitlab.com:phkg2/phrags/%s.git master\n' "${NEWNAME}"
	printf -- "Execute the command indicated when it finishes to add the new repo as the remote 'origin'."'\n'
	printf -- '\nNote, this repo will be PRIVATE until you change it using the web interface under heading:\n'
	printf -- '\tSettings->General->Permissions->Project Visibility\n'
	printf -- "Don't forget to save your settings after modifying them!"'\n'

}

# Merge phrag phkit into dest repo.
_phkg2_phrag_merge() {
	# main
	if [ $# -eq 3 ] ; then
		. "${MYSCRIPT%.sh}-merge.sh"
		merge_phrag_phkit_branch "$(_phkg2_name_with_phrag "${1}")" "${2}" "${3}"
	else
		printf -- '\nUsage: %s merge <phrag> <phkit> <branch>\n\n' "$0"
	fi
}

# Main dispatch function for phkg2_phrag
phkg2_phrag() {
	if [ $# -eq 0 ] ; then
		printf -- '\nUsage: %s <cmd> [<args..>]\n' "$(basename "$0" )"
		printf -- '\nCommands:\n'
		printf -- '\tclone        - Create a new phrag from a template.\n'
		printf -- '\tmerge        - Create merged g2 dest repo from phkits.\n'
		printf -- '\n'
		return 1
	else
		mycmd="${1}" ; shift
		case "${mycmd}" in
		clone) 
			phkg2_phrag_clone "$@"
		;;
		merge)
			_phkg2_phrag_merge "$@";;
		esac
	fi
}

# Setup basics
: "${PHKG2_ROOT:="/var/git/phkg2"}"
: "${PHRAG_ROOT:="${PHKG2_ROOT}/phrags"}"
: "${PHRAG_TEMPLATE_DEFAULT:="skeleton"}"
: "${PHKG2_PATCH_DEST_ROOT:="/var/tmp/phkg2/patches"}"
: "${G2_SRC_REPO_ROOT:="${PHKG2_ROOT}/g2-source-repos"}"
: "${G2_DEST_REPO_ROOT:="${PHKG2_ROOT}/g2-dest-repos"}"
: "${G2_SRC_REPO_MAP_FILE:="${G2_SRC_REPO_ROOT}/repo.map"}"

## Main:

if [ -z "${MYSCRIPT}" ] ; then
	MYSCRIPT="$(realpath "$0")"
	SCRIPTROOT="$(dirname "${MYSCRIPT}")"
	phkg2_phrag "$@"
fi

