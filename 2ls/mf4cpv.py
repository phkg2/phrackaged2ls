#/usr/bin/env python3
# args: reponame reporoot category package version

import sys
import os
import portage

from portage.dbapi.porttree import _parse_uri_map
if len(sys.argv) != 7 :
    print("Usage: %s <hashtype> <reponame> <reporoot> <category> <package> <version>")
    exit(1)
myhashtype = sys.argv[1]
myreponame = sys.argv[2]
myrepopath = sys.argv[3]
mycat=sys.argv[4]
mypkg=sys.argv[5]
myver=sys.argv[6]

mycp="%s/%s" % (mycat, mypkg)
mycpv="%s/%s-%s" % (mycat, mypkg, myver)

mypkgroot = "%s/%s" % (myrepopath, mycp )

myenv = os.environ.copy()
repos_conf = myenv['PORTAGE_REPOSITORIES'] = """
[DEFAULT]
main-repo = %s

[%s]
location = %s
""" % (myreponame, myreponame, myrepopath)

mypdb = portage.portdbapi(mysettings=portage.config(env=myenv, config_profile_path=''))
myeapi, myuris = mypdb.aux_get(mycpv, ["EAPI", "SRC_URI"])

my_fetchlist = dict()
my_fetchlist[mycpv] = _parse_uri_map(mycpv, {'EAPI':myeapi,'SRC_URI':myuris}, use=None)
#print(my_fetchlist)
#print('\n')

mymf = portage.Manifest(mypkgroot, fetchlist_dict=my_fetchlist)
#print(mymf.pkgdir)
#print('\n')

mydf = mymf._getCpvDistfiles(mycpv)
#print(mydf)
#print('\n')

myfdh = dict()
for myf in mydf:
    myfdh[myf]=mymf.fhashdict["DIST"][myf]
    #print(myfdh[myf])
    #print('\n')
    print('DIST %s %u %s %s' % ( myf, myfdh[myf]["size"], myhashtype, myfdh[myf][myhashtype] ))
