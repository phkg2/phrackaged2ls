#!/bin/sh
# Simple-stupid extract and merge script.



#
# Must have variable G2_SRC_REPO_MAP_FILE defined before calling any of the following.
#

## src_repos.lists and repos.map
# repos.map format: <repo_name> <repo_root> <repo_uri>
# src_repos.list format: <repo_name> <repo_uri>

# Read repos.map file with path ${G2_SRC_REPO_MAP_FILE} for source repos and print contents.
read_g2_src_repo_maps() {
	if [ -f "${G2_SRC_REPO_MAP_FILE}" ] ; then
		local mystr="$(cat "${G2_SRC_REPO_MAP_FILE}")"
		[ $? -eq 0 ] || die "Failed to read src repo map file '${G2_SRC_REPO_MAP_FILE}'."
		printf -- '%s' "${mystr}"
	else
		die "No src repo map found at '${G2_SRC_REPO_MAP_FILE}', please create. (FIXME - Should be automatic!)"
	fi
}

# Read src_repo.list file from the specified frag and print the contents.
read_phrag_repo_list() {
	local myphragdir="${1}"
	local myrepolistfile="${myphragdir}/src_repo.list"
	if [ -f "${myrepolistfile}" ] ; then
		local mystr="$(cat "${myrepolistfile}")"
		[ $? -eq 0 ] || die "Failed to read src repo list file '${myrepolistfile}'."
		printf -- '%s' "${mystr}"
	else
		die "No src repo list found at '${myrepolistfile}'."
	fi
}

# Merge new repos from indicated phrag's src_repo.list file to ${G2_SRC_REPO_MAP_FILE} without overwriting already defined (possibly modified) repos.
src_repo_merge_phrag_repo_list() {
	local myphragdir="${1}"
	[ -d "${G2_SRC_REPO_MAP_FILE%/*}" ] || mkdir -p "${G2_SRC_REPO_MAP_FILE%/*}" || die "Couldn't create directory for src repo map '${G2_SRC_REPO_MAP_FILE%/*}'."
	[ -f "${G2_SRC_REPO_MAP_FILE}" ] || touch "${G2_SRC_REPO_MAP_FILE}" || die "Couldn't create source repo map '${G2_SRC_REPO_MAP_FILE}'."
	local mystrmap="$(read_g2_src_repo_maps)"
	local mystrlist="$(read_phrag_repo_list "${myphragdir}" )"
	[ $? -eq 0 ] || die "Failed to read repo list."
	local mystr="${mystrmap:+$(printf -- '%s\n' "${mystrmap}")}${mystrlist}"
	printf -- '%s' "${mystr}" | awk 'BEGIN { OFS="\t"; };
		/^# REPO_NAME/ { if (NR==1) {print;}; next;};
		/^[[:space:]]*#/ { print; next;};
		NF==3 { repos[$1]=$1; print; };
		NF==2 { if ( !repos[$1] ) { print $1, $1, $2; }; };' > "${G2_SRC_REPO_MAP_FILE}"
}

#
# REPOREF handling functions
#

# reporef format: <reponame>[/<subdir>][@<gitref>]  where gitref may be a git branch, tag, commit id, etc.


# Get the value of the requested field from the ${G2_SRC_REPO_MAP_FILE}
get_reporef_field() {
	local repo_name="$(get_reporef_name "${1}")"
	field_name="${2}"
	case "${field_name}" in
		name) field_num=1;;
		root) field_num=2;;
		uri) field_num=3;;
		*) echo "Bad field: '${field_name}' for repo '${repo_name}'! Should be 'nane', 'root', or 'uri'." ; return 1 ;;
	esac
	cat "${G2_SRC_REPO_MAP_FILE}" | awk '$1=="'"${repo_name}"'" { print $'"${field_num}"' }'
}


# Get just the repo name, hacking off any path passed after it.
get_reporef_name() { local n="${1%@*}" ; printf -- "${n%%/*}" ; }

# Get just the subdir under the repo-root, if given.
get_reporef_subdir() { local s="${1%@*}" ; [ "${s}" == "${s#*/}" ] || printf -- "${s#*/}" ; }

# Get just the git-ref (commit/tag/etc) under the repo-root, if given, follows @ sign. Default to 'master' if none given.
get_reporef_gitref() { [ "${1}" == "${1##*@}}" ] && printf -- "master" || printf -- "${1##*@}" ; }

# Get a full path in the repo.
get_reporef_path() {
	local repo_root="$(get_reporef_root "${1}")"
	local repo_subdir="$(get_reporef_subdir "${1}")"
	printf -- "${repo_root}${repo_subdir:+/${repo_subdir}}"
}

# Get the repo root, prefix relative paths with ${G2_SRC_REPO_ROOT}
get_reporef_root() {
	local repo_name="$(get_reporef_name "${1}")"
	local repo_root="$(get_reporef_field "${repo_name}" "root")"
	case "${repo_root}" in
		"."/*|".."/*|"/"*) printf -- "${repo_root}" ;;
		[_[:alnum:]]*) printf -- "${G2_SRC_REPO_ROOT}/${repo_root}" ;;
		*) return 1 ;;
	esac
}

# Get a commit hash for the given reporef
get_reporef_hash() {
	local myrr="${1}" ; shift
	local myrepogitref="$(get_reporef_gitref "${myrr}")"
	local myreporoot="$(get_reporef_root "${myrr}")"
	local myreposubdir="$(get_reporef_subdir "${myrr}")"
	local myrepopath="${myreporoot}${myreposubdir:+/${myreposubdir}}"
	[ -d "${myreporoot}/.git" ] || die "Tried to get commit hash for '${myrepogitref}', but '${myreporoot}' isn't a git repo."
	( set -e
		cd "${myreporoot}"
		git rev-parse "${myrepogitref}"
	) || die "Could not get commit hash for '${myrepogitref}' in '${myrepopath}'."
}

# Get the uri for the repo.
get_reporef_uri() { get_reporef_field "${1}" "uri" ; }



#PHKIT_LIST_FILE="phkit.list"
#KIT_FILE_ROOT="kits"
#
#
# Load list of kits to generate
#[ -f "${KIT_LIST_FILE}" ] || die "No file '${KIT_LIST_FILE}' to read list of kits to generate!"
#while read -r mykit; do
#	case "${mykit}" in
#		"#"*)
#			# Comment, do nothing
#			:
#		;;
#		[_[:alnum:]]*)
#			# Add this kit to list of kits to generate
#			KITLIST="${KITLIST:+${KITLIST} }${mykit}"
#		;;
#	esac
#done < "${KIT_LIST_FILE}"


# Iterate over the list of kits in KITLIST, extracting branches and merging all branches for each, in order.
#for mykit in ${KITLIST} ; do
#	mykitfile="${KIT_FILE_ROOT:+${KIT_FILE_ROOT%/}/}${mykit}.kit"
#	# Handle kits with sub paths
#	if [ "${mykit}" = "${mykit%/*}" ] ; then
#		mykitname="${mykit}"
#		mykitsub=""
#	else
#		mykitname="${mykit##*/}"
#		mykitsub="${mykit%/*}"
#	fi
#done


# Setup a source repo if it doesn't exist.
src_repo_setup() {
	local repo_name="$(get_reporef_name "${1}")"
	local repo_root="$(get_reporef_root "${repo_name}")"
	local repo_uri="$(get_reporef_uri "${repo_name}")"
	if ! [ -d "${repo_root}" ] ; then
		printf -- '\nCloning new source repo "%s" from %s to %s.\n' "${repo_name}" "'${repo_uri}'" "'${repo_root}'"
		mkdir -p "${repo_root%/*}"
		git clone "${repo_uri}" "${repo_root}"
	else
		printf -- '\nUpdating source repo %s in %s.\n' "'${repo_name}'" "'${repo_root}'"
		( cd "${repo_root}" && git pull )
		: # echo "To update gentoo repo: cd ../source-trees/gentoo && git pull"
	fi
}


## Dest repo handling

# dest_repo_root
_dest_repo_clear() {
	mydestreporoot="${1}"

	# Wipe our repo dir so we can start from scratch.
	# Complain and bail if we find something other than a git root we setup (MT branch exists).
	if [ -e "${mydestreporoot}" ] ; then
		if [ -d "${mydestreporoot}/.git" ] ; then
			if ( cd "${mydestreporoot}" && git branch | grep  -q '^[*[:space:]]*MT[[:space:]]*$' ) ; then
				printf -- "Removing old dest git repo at '${mydestreporoot}'.\n"
				rm -rf "${mydestreporoot}" || die "Could not remove old git repo '${mydestreporoot}'!"
			else
				die "'${mydestreporoot}' looks like a git repo, but not one we created (no 'MT' branch), BAIL-OUT!"
			fi
		else
			die "'${mydestreporoot}' is not a git repo root, not touching it and bailing!"
		fi
	fi

	# Initilize our repo from scratch.
	printf -- '\nCreating new dest repo in %s.\n' "'${mydestreporoot}'"
	mkdir -p "${mydestreporoot}" || die "Could not create directory '${mydestreporoot}'."
	( set -e
		cd "${mydestreporoot}" 
		git init .
		git checkout -B MT
		touch .gitignore
		git add .gitignore
		git commit -m "Empty .gitignore root commit."
	) || die "Could not initilize new dest repo at '${mydestreporoot}'."
}


# dest_repo_root
dest_repo_setup() {
	mydestreporoot="${1}"
	[ -d "${mydestreporoot}/.git" ] || _dest_repo_clear "${mydestreporoot}" || die "Failed to setup dest repo at '${mydestreporoot}'."
}


# dest_repo_root, dest_branch
_dest_repo_branch_clear() {
	mydestreporoot="${1}"
	mydestbranch="${2}"

	printf -- '\nClearing branch "%s" in dest repo at %s.\n' "${mydestbranch}"  "'${mydestreporoot}'"
	dest_repo_setup "${mydestreporoot}"

	( set -e
		cd "${mydestreporoot}" || die "Could not change to '${mydestreporoot}'!"
		git checkout -f -B "${mydestbranch}" MT
		git reset --hard
	) || die "Failed to clear branch \"${mydestbranch}\" in '${mydestreporoot}'!"
}


## Patch extraction and merging

# repo_ref, patchfile, globlist...
_rr_extract_patch() {
	local myrr="${1}" ; shift
	# Setup/update src repo.
	src_repo_setup "${myrr}" || die "Failed to setup source repo for '${myrr}'."

	local mypatch="${1}" ; shift
	local myrepogitref="$(get_reporef_gitref "${myrr}")"
	local myreponame="$(get_reporef_name "${myrr}")"
	local myreporoot="$(get_reporef_root "${myrr}")"
	local myreposubdir="$(get_reporef_subdir "${myrr}")"
	local myrepopath="${myreporoot}${myreposubdir:+/${myreposubdir}}"


	# Fetch our hash for the git_ref
	local myrepohash="$(cd "${myrepopath}" && git rev-parse "${myrepogitref}" )"
	[ $? -eq 0 ] || die "Could not get commit hash for '${myrepogitref}' in '${myrepopath}'."

	printf -- '\nExtracting patch from source repo "%s" for reporef %s,\nto %s,\nfor the following globs:\n' "${myreponame}" "'${myrepogitref}'" "'${mypatch}'"
	printf -- '\t%s\n' "$@"
	printf -- '\n(This may take a while on large repos with many patterns.)\n'

	( set -e
		cd "${myrepopath}"
		set -f
		#git log \
		#	--binary --pretty=email --patch-with-stat --topo-order --reverse --full-index ${myreposubdir:+--relative} \
		#	--break-rewrites=40%/70% --find-renames=20% --remove-empty \
		#	${myrepohash} -- "$@"
		#git log \
		#	--patch-with-stat --binary --format=email --ignore-missing --topo-order --reverse --full-index ${myreposubdir:+--relative} \
		#	--remove-empty \
		git format-patch -k --stdout --full-index --binary --thread --no-renames ${myreposubdir:+--relative} \
			--root ${myrepohash} --topo-order -- "$@"
	) > "${mypatch}"
	[ $? -eq 0 ] || die "Failed while generating '${mypatch}' from reporef '${myrr}' with repo in '${myreporoot}'."
	printf -- 'Generated %s successfully.\n' "'${mypatch}'"
}


# dest_repo_root, dest_branch, patch
_dest_repo_merge_patch() {
	local mydestreporoot="${1}"
	local mydestbranch="${2}"
	local mypatch="${3}"

	dest_repo_setup "${mydestreporoot}"

	( set -e
		cd "${mydestreporoot}" || die "Could not change to '${mydestreporoot}'!"



		# Skip empty patches (non-existent or zero len) to avoid git am errors.
		if ! [ -s "${mypatch}" ] ; then
			printf -- "\nSkipping empty patchset '${mypatch}'.\n"
			return 0
		fi

		# Give the user an idea what's going on ;)
		printf -- "\nMerging patchset '${mypatch}' to branch '${mydestbranch}' in '${mydestreporoot}'.\n"

		# Create new branch for each patcheset
		git checkout MT || die "Failed to check out 'MT' branch."
		git reset --hard
		git checkout -B patch || die "Failed to create new empty branch 'patch'."

		# Apply our patchset to that branch
		am_done=0
		am_res=0

		git am -k --keep-cr --whitespace=nowarn --keep-non-patch "${mypatch}" || am_res=$?

		# As long as we have patches which need skipping, skip them...
		while [ ${am_res} -eq 128 ] ; do
				printf -- '\nSkipping...\n'
				am_res=0
				git am --skip || am_res=$?
		done

		if [ ${am_res} -ne 0 ] ; then
			die "Failed to git am '${mypatch}'."
		fi

		# Switch to target branch (Creating from MT if it doesn't exist).
		if git branch --list | grep  -q '^[*[:space:]]*'"${mydestbranch}"'[[:space:]]*$' ; then
			git checkout "${mydestbranch}"
		else
			git checkout MT
			git checkout -B "${mydestbranch}"
		fi
		git reset --hard

		# Attempt to merge our new patcheset branch
		if git merge --no-commit patch ; then
			printf -- '\nMerged without conflict\n'
		else
			printf -- '\nList of conflicts needing resolution with --theirs merge:\n'
			git ls-files --unmerged
			# Resolve conflicts by always taking the version from the branch being merged in
			git ls-files --unmerged | awk '{print $NF;}' | sort -u | xargs -r git checkout --theirs --
			printf -- '\nConflicts resolved forcefully.\n'
		fi
		printf -- '\nCommiting changes to branch %s.\n' "'${mydestbranch}'"
		# Add changes and commit.
		git add .
		git commit -m "Merged '${mypatch##*/}' into '${mydestbranch}'."

		# Delete 'patch' branch.
		git branch -D patch
	)
}

# Extract patches from reporefs and apply to dest_repo (dest_branch) containing all files matched by globs.
# dest_repo_root, dest_branch, reporefs, globlist...
extract_and_merge_patches() {
	local mydestreporoot="${1}" ; shift
	local mydestbranch="${1}" ; shift
	local myrrs="${1}" ; shift
	local myglobs="$@"

	# Short-circuit return if we have nothign to do.
	[ -n "${myrrs}" ] && [ $# -gt 0 ] || return

	# Generate hash for this globlist for a unique, repeatable id string
	local myglobshash="$(printf -- '%s' "$@" | md5sum | cut -d' ' -f1)"

	local myrr
	for myrr in ${myrrs} ; do
		src_repo_setup "${myrr}" || die "Failed to setup repo for '${myrr}'."
		local myrrhash="$(get_reporef_hash "${myrr}")"
		local mypatchname="$(printf -- '%s' "${myrr}" | sed -e 's/[-/.]/_/g')-${myrrhash}-${myglobshash}.patch"

		# Setup our paths to store patches and dest git repo
		local mypatchdir="${PHKG2_PATCH_DEST_ROOT}/${mydestreporoot##*/}/${mydestbranch}"
		mkdir -p "${mypatchdir}" || die "Could not create directory for storing patches at '${mypatchdir}'."

		# Check if patch already exists, don't overwrite without FORCE_REEXTRACT_PATCHES set.
		local mypatch="${mypatchdir}/${mypatchname}"
		[ -n "${FORCE_REEXTRACT_PATCHES}" ] && rm -f "${mypatch}"
		[ -f "${mypatch}" ] || _rr_extract_patch "${myrr}" "${mypatch}" ${myglobs}
		_dest_repo_merge_patch "${mydestreporoot}" "${mydestbranch}" "${mypatch}"
	done

}

# dest_reporpoot dest_branch phkitfile
dest_repo_branch_merge_phkitfile() {
	local mydestreporoot="${1}"
	local mydestbranch="${2}"
	local myphkitfile="${3}"

	# Define our token to introduce a list of reporefs
	local REPOREFS_TOK="#REPOREFS="

	# Clear our globals before we start parsing kits.
	local myrrs=""
	local allglobs=""
	local mybranchwritten=0
	local myglob

	# Bail if our file doesn't exist.
	[ -f "${myphkitfile}" ] || die "No '${myphkitfile}' file found!"


	# Read lines from the current kit file as globs to add to our list to extract
	while read -r myglob; do
		# Parse this line
		case "${myglob}" in
			# Line starts with our REFOREFS token
			"${REPOREFS_TOK}"*)
				# If we're going to swtich to a new repo, we need to generate the patchset for the last one before we change anything.
				# Clear out our destination branch before the first time we write it (setting up repo if needed)
				# Extract and merge the last set of patches, then clear allglobs so we start fresh for the next reporef
				[ ${mybranchwritten} -gt 0 ] || _dest_repo_branch_clear "${mydestreporoot}" "${mydestbranch}"
				extract_and_merge_patches "${mydestreporoot}" "${mydestbranch}" "${myrrs}" "${allglobs}"
				mybranchwritten=$(($mybranchwritten+1))
				myrrs="${myglob#"${REPOREFS_TOK}"}"
				allglobs=""
			;;
			""|"#"*)
				# Found empty line or comment, do nothing using ':' command
				:
			;;
			[_.[:alnum:]]*)
				# Add this glob to the list of all globs to extract
				allglobs="${allglobs} ${myglob}"
			;;
		esac
	done < "${myphkitfile}"

	# We found the end of the file.
	# Clear out our destination branch before the first time we write it (setting up repo if needed)
	[ ${mybranchwritten} -gt 0 ] || _dest_repo_branch_clear "${mydestreporoot}" "${mydestbranch}"
	# Process the last set of reporefs seen with the current ${allglobs} value.
	extract_and_merge_patches "${mydestreporoot}" "${mydestbranch}" "${myrrs}" "${allglobs}"
}

#phrag_name phkit_name phkit_branch
merge_phrag_phkit_branch() {
	local myphragname="${1}"
	local myphkitname="${2}"
	local mydestbranch="${3}"

	local myphkitfilename="${myphkitname}@${mydestbranch}.phkit"
	local myphragdir="${PHRAG_ROOT}/${myphragname}"
	local myphkitdir="${myphragdir}/${myphkitname}"
	local myphkitfilepath="${myphkitdir}/${myphkitfilename}"

	local mydestreporoot="${G2_DEST_REPO_ROOT}/${myphkitname}"

	[ -f "${myphkitfilepath}" ] || die "Can't find '${myphkitfilepath}'."
	src_repo_merge_phrag_repo_list "${myphragdir}"
	dest_repo_branch_merge_phkitfile "${mydestreporoot}" "${mydestbranch}" "${myphkitfilepath}"


	# Setup metadata if not provided from phkit.
	local commit_needed=0

	local myprofilesdir="${mydestreporoot}/profiles"
	mkdir -p "${myprofilesdir}" || die "Couldn't create directory '${mprofilesdir}'."

	local mymetadatadir="${mydestreporoot}/metadata"
	mkdir -p "${mydestreporoot}/metadata" || die "Couldn't create directory '${mydestreporoot}/metadata'."

	if ! [ -f "${myprofilesdir}/repo_name" ] ; then
		printf -- '%s\n' "${myphkitname}" > "${myprofilesdir}/repo_name"
		( cd "${mydestreporoot}" && git add -- ./profiles/repo_name )
		commit_needed=1
	fi
	if ! [ -f "${myprofilesdir}/categories" ] ; then
		local mycats="$( cd "${mydestreporoot}" && echo *-*/*/*.ebuild | sed -e 'y/ /\n/' | cut -d/ -f1 | sort -u | sed -e '/^$/ d' )"
		[ -n "${mycats}" ] && printf '%s\n' "${mycats}" > "${myprofilesdir}/categories"
		( cd "${mydestreporoot}" && git add -- ./profiles/categories )
		commit_needed=1
	fi
	if ! [ -f "${mymetadatadir}/layout.conf" ] ; then
		cat > "${mymetadatadir}/layout.conf" <<-EOF
			repo-name = ${myphkitname}
			masters = ${DEST_REPO_DEFAULT_MASTERS}
			thin-manifests = true
			sign-manifests = false
			profile-formats = portage-2
			cache-formats = md5-dict
		EOF
		( cd "${mydestreporoot}" && git add -- ./metadata/layout.conf )
		commit_needed=1
	fi

	[ ${commit_needed} -eq 1 ] && 	( cd "${mydestreporoot}" && git commit -m "Add generated metadata." )

	# Temporary hack to fetch distfiles and digest:
	if [ -n "${FORCE_EBUILD_DIGEST}" ] ; then
		local ebp
		local manup=0
		for ebp in $( cd "${mydestreporoot}" && find ./ -iname '*.ebuild' | sort -u -t / -k 1,3 | cut -f2- -d/ ) ; do
			( cd "${mydestreporoot}/${ebp%/*}" && ebuild "${ebp##*/}" digest || die "Could not generate digest for '${mydestreporoot}/${ebp%/*}'." )
			manup=1
		done
		[ ${manup} -eq 1 ] && ( cd "${mydestreporoot}" && git add ./*-*/*/Manifest && git commit -m "Add generated Manifests" )
	fi
}


# Merge phrag phkit into dest repo.
phkg2_phrag_merge() {
	# Bail out! function -- Defined here to avoid overwriting die() provided by parent script if sourced.
	die() {
		printf -- '%s\n' "$*"
		exit 1
	}

	# main
	if [ $# -eq 3 ] ; then
		if ! [ -d "${PHRAG_ROOT}/${1}" ] || [ -n "${FORCE_PHRAG_PULL}" ] ; then
			source "${SCRIPTROOT}"/phkg2-phrag-getit.sh "${1}"
			phkg2_phrag_getit "${1}"
		fi
		merge_phrag_phkit_branch "${1}" "${2}" "${3}"
	else
		printf -- '\nUsage: %s <phrag> <phkit> <branch>\n\n' "$0"
	fi
}


# Setup basics, these will be overridden if already set.
: "${PHKG2_ROOT:="/var/git/phkg2"}"
: "${PHRAG_ROOT:="${PHKG2_ROOT}/phrags"}"
: "${PHKG2_PATCH_DEST_ROOT:="/var/tmp/phkg2/patches"}"
: "${G2_SRC_REPO_ROOT:="${PHKG2_ROOT}/g2-source-repos"}"
: "${G2_DEST_REPO_ROOT:="${PHKG2_ROOT}/g2-dest-repos"}"
: "${G2_SRC_REPO_MAP_FILE:="${G2_SRC_REPO_ROOT}/repo.map"}"
: "${DEST_REPO_DEFAULT_MASTERS:="core-kit"}"

# If we're not being sourced from another script which already defined it, set MYSCRIPT,SCRIPTROOT and run our "main" function
if [ -z "${MYSCRIPT}" ] ; then
	MYSCRIPT="$(realpath "$0")"
	SCRIPTROOT="$(dirname "${MYSCRIPT}")"
	phkg2_phrag_merge "$@"
fi

