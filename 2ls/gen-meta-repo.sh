die() {
	printf -- '\n%s\n' "$*"
	exit 1
}

_get_repo_root_git_branches_with_commits() {
	local myreporoot="${1}"
	cd "${myreporoot}" || die
	git branch --list --no-abbrev -v | awk '/(HEAD)/ || / MT / {next;}; {$0=substr($0,3); print $1,$2; };'
	cd - 2>&1 > /dev/null
}

_git_branches_to_kits_sha1_json_entry() {
	awk -v kitname="${1##*/}" \
		'BEGIN { printf("\t\"%s\": {", kitname); };
		{ printf("%s\n\t\t\"%s\": \"%s\"", (NR>1? ",":""), $1, $2); };
		END { printf( "\n\t}" ); }'
}

git_repo_root_branches_stability_entry() {
	local myreporoot="${1}"
	shift
	printf -- '\t\t\t"stability": {\n'
	while [ $# -gt 0 ] ; do
		local mybranchstability="$(cd "${myreporoot}" && git cat-file -p "${1}":./metadata/stability 2> /dev/null || printf -- 'current')"
		printf -- '\t\t\t\t"%s": "%s"' "${1##*/}" "${mybranchstability}"
		shift && [ $# -gt 0 ] && printf -- ','
		printf -- '\n'
	done
	printf -- '\t\t\t}'
}

git_repo_roots_to_kits_sha1_json() {
	[ $# -gt 0 ] || die "Usage: $0 kits-sha1.json <list of: repo_root...>"
	printf -- '{\n'
	while [ $# -gt 0 ] ; do
		_get_repo_root_git_branches_with_commits "${1}" | _git_branches_to_kits_sha1_json_entry "${1}"
		shift && [ $# -gt 0 ] && printf -- ','
		printf -- '\n'
	done
	printf -- '}\n'
}

git_repo_roots_to_kit_info_json_kit_settings() {
	printf -- '\t"kit_settings": {\n'
	while [ $# -gt 0 ] ; do
		printf -- '\t\t"%s": {\n' "${1##*/}"
		git_repo_root_branches_stability_entry "${1}" $(_get_repo_root_git_branches_with_commits "${1}" | cut -d' ' -f1)
		printf -- ',\n\t\t\t"type": "auto"\n'
		printf -- '\t\t}'
		shift && [ $# -gt 0 ] && printf -- ','
		printf -- '\n'
	done
	printf -- '\t}'
}

git_repo_roots_to_kit_info_json_kit_order() {
	printf -- '\t"kit_order": [\n'
	while [ $# -gt 0 ] ; do
		printf -- '\t\t"%s"' "${1##*/}"
		shift && [ $# -gt 0 ] && printf -- ','
		printf -- '\n'
	done
	printf -- '\t]'
}

git_repo_roots_with_branches_to_kit_info_json_release_defs() {
	printf -- '\t"release_defs": {\n'
	while [ $# -gt 0 ] ; do
		local myrr="${1%%@*}"
		local mybr="${1#*@}"
		[ "${mybr}" = "${myrr}" ] && mybr=""
		printf -- '\t\t"%s": [' "${myrr##*/}"
		(
			# If the last branch is specified as '*', include all remaining branches in repo following specified branches.
			if [ "${mybr%,\*}" != "${mybr}" ] ; then
				mybr="${mybr%,\*}"
				# This prints our desired list of branches in order they appear.
				(IFS=","; printf -- '%s\n' ${mybr} )
				# This chunk grabs the list of the remaining branches for the repo.
				( set -e
					# This makes sure we don't have duplicates show up if a branch doesn't exist.
					( set -f; IFS=","; printf -- '%s\n' ${mybr} ; printf -- '%s\n' ${mybr} )
					_get_repo_root_git_branches_with_commits "${myrr}" | cut -d' ' -f1
				) | sort -V | uniq -u
			# Include only specified branches when given.
			elif [ -n "${mybr}" ] ; then
				(IFS=","; printf -- '%s\n' ${mybr})
			# Othwise, include all branches in repo.
			else
				_get_repo_root_git_branches_with_commits "${myrr}" | cut -d' ' -f1
			fi
		) | awk '{ printf("%s\n\t\t\t\"%s\"", (NR>1? ",":""), $1); }; END { printf("\n");};'
		printf -- '\t\t]'
		shift && [ $# -gt 0 ] && printf -- ','
		printf -- '\n'
	done
	printf -- '\t}'
}


kit_info_json_release_info() {
	local myrel="${1}"
	local mybasekit="${2%%@*}" ; mybasekit="${mybasekit##*/}"
	printf -- '\t"release_info": {\n'
	printf -- '\t\t"default": "%s"\n' "${myrel}"
	[ -n "${mybasekit}" ] && printf -- '\t\t"base_kit": "%s"\n' "${mybasekit}"
	printf -- '\t}'
}

gen_metarepo_metadata() {
	mycmd="${1}"
	shift

	case "${mycmd}" in
		kits-sha1.json)
			git_repo_roots_to_kits_sha1_json "${@%@*}"
		;;
		kits-info.json)
			myrel="${1}"
			shift
			printf -- '{\n'
			git_repo_roots_to_kit_info_json_kit_order "${@%@*}"
			printf -- ',\n'
			git_repo_roots_to_kit_info_json_kit_settings "${@%@*}"
			printf -- ',\n'
			git_repo_roots_with_branches_to_kit_info_json_release_defs "${@}"
			printf -- ',\n'
			kit_info_json_release_info "${myrel}" "${1}"
			printf -- '\n}\n'
		;;
		version.json)
			printf -- '{\n\t"required": {\n\t\t"ego": "2.7.2"\n\t},\n\t"version": 11\n}\n'
		;;
		*)
			printf -- '\nUsage: %s [kits-sha1.json | kits-info.json <release>] <list of: repo_root[@branch]>\n' "${0##*/}"
		;;
	esac
}

# <repo_root> [priority]
gen_repos_conf_entry() {
	local myrr="${1}"
	local myprio="${2:-1}"
	printf -- '[%s]\n' "${myrr##*/}"
	printf -- 'location = %s\n' "${METAREPOKITROOT:-${METAREPOROOT:-/var/git/meta-repo}/kits}/${myrr##*/}"
	printf -- 'auto-sync = no\n'
	printf -- 'priority = %s\n' "${myprio}"
}

myoutcmd() {
	outfile="${1}" shift
	printf -- '\ncat > "%s" <<EOF\n' "${outfile}"
	"$@" | cat
	printf -- 'EOF\n'
}


myver="${1}"
shift
updatesha1=0
if [ "${myver}" = "--update-sha1" ] ; then
	updatesha1=1
	myver="${1}"
	shift
fi


[ $# -gt 1 ] || die "$(printf -- 'Error: No repos to process.\nUsage: '"${0##*/}"'[--update-sha1] <release_version> <list of: repo_root[@branch[,branch...[,*]]]>\n')"


printf -- '# Run the following in meta-repo base directory:\n'
if [ ${updatesha1} -eq 1 ] ; then
	printf -- 'git checkout %s\n' "${myver}"
	printf -- 'mkdir -p metadata\n'
	myoutcmd metadata/kits-sha1.json gen_metarepo_metadata kits-sha1.json "$@"
	printf -- 'git add metadata/kits-sha1.json\n'
	printf -- 'git commit -m "Updated kits-sha1.json for branch %s.\n' "'${myver}'"
else
	printf -- '\ngit checkout -B %s\n' "${myver}"
	printf -- 'mkdir -p metadata\n'
	myoutcmd metadata/kits-sha1.json gen_metarepo_metadata kits-sha1.json "$@"
	printf -- '\ngit add metadata/kits-sha1.json\n'
	myoutcmd metadata/kits-info.json gen_metarepo_metadata kits-info.json "${myver}" "$@"
	printf -- '\ngit add metadata/kits-info.json\n'
	myoutcmd metadata/version.json gen_metarepo_metadata version.json "$@"
	printf -- '\ngit add metadata/version.json\n'

	printf -- 'mkdir -p repos.conf\n'
	for r in "${@%%@*}" ; do
		myoutcmd repos.conf/kit-${r##*/}.conf gen_repos_conf_entry "${r}"
		printf -- '\ngit add repos.conf/%s\n' "kit-${r##*/}.conf"
	done
	printf -- '\ngit commit -m "Wrote meta-repo metadata and repos.conf entries for branch %s."\n' "'${myver}'"
fi
