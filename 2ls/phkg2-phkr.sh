

_read_phkr_file() {
	[ -f "${1}" ] || return 1
	local myphkrfile="${1}" ; shift
	local callback="${1}"
	local line

	# Read lines from file, only printing those starting with 'phrag-'
	while read -r line ; do
		case "${line}" in
			\#*) continue ;;
			phrag-*) ${callback:-printf -- '%s\n'} "${line}"  ;;
			*) continue ;; # Add handling for unknown lines later.
		esac
	done < "${myphkrfile}"
}

_split_phrag_phkit_vers() {
	local myphragname="${1%%/*}"
	local myphkitname="${1#${myphragname}/}" ; myphkitname="${myphkitname%@*}"
	local myphkitverlist="${1##*@}" ; myphkitverlist="${myphkitverlist%,\*}"
	local myphkitver
	for myphkitver in $(IFS=","; printf -- '%s\n' ${myphkitverlist}) ; do
		printf '%s %s %s\n' "${myphragname}" "${myphkitname}" "${myphkitver}"
	done
}

phkg2_phkr_usage() {
cat <<EOF
Usage: ${1:-${0}} [--up] [--deep] <phkrfile>

	Build a release as specified by <phkrfile>.

	Options:
	--up	Force repo update
	--deep	Force pull phrags and update repos

EOF
}

phkg2_phkr() {

	# Handle updating repos
	if [ "x${1}" = "x--up" ] ; then
		shift
		myphkrup=1
	# Pull phrags, then update repos
	elif [ "x${1}" = "x--deep" ] ; then
		shift
		myphkrup=1
		FORCE_PHRAG_PULL=1
	# Otherwise, just pull missing repos and generate meta-repo
	else
		unset myphkrup
	fi

	if [ $# -lt 1 ] ; then
		phkg2_phkr_usage
		exit 1
	fi

	myphkrfile="$(realpath "${1}")" ; shift
	myphkrver="${myphkrfile##*@}" ; myphkrver="${myphkrver%.phkr}"
	myphkrname="${myphkrfile##*/}" ; myphkrname="${myphkrname%%@*}"

	# Loop over list of phrags/phkits@vers and pull missing (or all if forced)
	(
		_read_phkr_file "${myphkrfile}" _split_phrag_phkit_vers | while read -r myphragname myphkitname myphkitver ; do
			if [ -d "${G2_DEST_REPO_ROOT}/${myphkitname}" ] ; then
				printf -- 'Dest repo %s already exists for %s.\n'  "'${G2_DEST_REPO_ROOT}/${myphkitname}'"  "'${myphragname}/${myphkitname}@${myphkitver}'"
				if [ -z "${myphkrup}" ] ; then printf 'Skipping.\n' ; continue ; else printf 'Updating...\n' ; fi
			fi
			mycmd="sh "${SCRIPTROOT}"/phkg2-phrag-merge.sh ${myphragname} ${myphkitname} ${myphkitver}"
			printf -- '%s\n' "${mycmd}"
			[ -n "${FORCE_PHRAG_PULL}" ] && export FORCE_PHRAG_PULL=1
			${mycmd}
		done
	)

	printf -- '\n\n'
	local myphkroutroot="${F2_DEST_METAPHKR_ROOT}/${myphkrname}"

	if ! [ -d "${myphkroutroot}/meta-repo" ] ; then
		mkdir -p "${myphkroutroot}/meta-repo" || die "Could not destination directory '${myphkroutroot}/meta-repo' for f2 meta-repo flavor release '${myphkrname}@${myphkrver}'!"
		( cd "${myphkroutroot}/meta-repo" && git init . ) || die "Could not initilize new git repo in '${myphkroutroot}/meta-repo'!"
	elif ! [ -d "${myphkroutroot}/meta-repo/.git" ] ; then
		die "Found existing directory at '${myphkroutroot}/meta-repo', but it doesn't appear to be a git directory -- bailing out!"
	fi
	# Change to our output directory	
	( cd "${myphkroutroot}"/meta-repo || die "Failed to change to output directory '${myphkroutroot}/meta-repo!"
(cat | sh) <<EOF
		$( cd "${G2_DEST_REPO_ROOT}"
			sh "${SCRIPTROOT}"/gen-meta-repo.sh "${myphkrver}" $(_read_phkr_file "${myphkrfile}" | cut -d/ -f2- ) # | grep -v -e "^#" -e "^$"
		) 
EOF
	)
}

: "${PHKG2_ROOT:="/var/git/phkg2"}"
: "${PHRAG_ROOT:="${PHKG2_ROOT}/phrags"}"
: "${PHKG2_PATCH_DEST_ROOT:="/var/tmp/phkg2/patches"}"
: "${G2_SRC_REPO_ROOT:="${PHKG2_ROOT}/g2-source-repos"}"
: "${G2_DEST_REPO_ROOT:="${PHKG2_ROOT}/g2-dest-repos"}"
: "${F2_DEST_METAPHKR_ROOT:="${PHKG2_ROOT}/f2-dest-metaphkrs"}"

if [ -z "${MYSCRIPT}" ] ; then
	MYSCRIPT="$(realpath "$0")"
	SCRIPTROOT="$(dirname "${MYSCRIPT}")"
	phkg2_phkr "${@}"
fi

