
phkg2_phrag_getit() {
	if [ "${1}" = "all" ] ; then
		set --
		phkg2_phrag_getit_all
	fi

	while [ $# -gt 0 ] ; do
		local myphrag="phrag-${1#phrag-}" ; shift
		local myphragdir="${PHRAG_ROOT}/${myphrag}"
		local myphraguri="${PHRAG_GIT_BASE_URI}/${myphrag}"
		[ -e "${PHRAG_ROOT}" ] && ! [ -d "${PHRAG_ROOT}" ] && die "PHRAG_ROOT '${PHRAG_ROOT}' exists, but is not a directory!"
		if ! [ -d "${myphragdir}" ] ; then
			mkdir -p "${PHRAG_ROOT}" || die "Could not create directory for PHRAG_ROOT at '${PHRAG_ROOT}'!"
			( cd "${PHRAG_ROOT}" && git clone "${myphraguri}" ) || die "Cloning '${myphrag}' from '${myphraguri}' to '${myphragdir}' failed!"

		elif [ -d "${myphragdir}/.git" ] ; then
			( cd "${myphragdir}" && git pull ) || die "git pull for phrag '${myphrag}' in '${myphragdir}' failed!"
		else
			die "Directory '${myphragdir}' exists, but is not a git repo. Did you forget to run 'git init' it?"
		fi
	done
}

phkg2_phrag_getit_all() {
	phkg2_phrag_getit $( curl -D - -s "https://gitlab.com/api/v4/groups/phkg2%2Fphrags" \
		| awk 'BEGIN { RS=","; FS=":" ; }; /path_with_namespace/ {print}' \
		| sed -e 's/\("path_with_namespace":"\)\([^"]\+\)".*/\2/' \
		| sed -n -e 's|phkg2/phrags/||gp'
	)
}

# Setup basics
: "${PHKG2_ROOT:="/var/git/phkg2"}"
: "${PHRAG_ROOT:="${PHKG2_ROOT}/phrags"}"

# Default to https for cloning, allow override using PHKG2_GIT_PROTO.
# Specifying PHKG2_GIT_BASE_URI explicitly overrides above.
case "${PHKG2_GIT_PROTO:=https}" in
	https|https|git)
		: "${PHKG2_GIT_BASE_URI:="${PHKG2_GIT_PROTO}://git:git@gitlab.com/phkg2"}"
	;;
	ssh)
		: "${PHKG2_GIT_BASE_URI:="ssh://git@gitlab.com/phkg2"}"
	;;
esac

: "${PHRAG_GIT_BASE_URI:="${PHKG2_GIT_BASE_URI}/phrags"}"

if [ -z "${MYSCRIPT}" ] ; then
	MYSCRIPT="$(realpath "$0")"
	SCRIPTROOT="$(dirname "${MYSCRIPT}")"

	# Helper to die nicely.
	die() {
		printf -- '\n%s\n' "$*"
		exit 1
	}

	phkg2_phrag_getit "$@"
fi

