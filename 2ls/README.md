# Scripts that are used to generate stuff around phkg2

## gen-meta-repo.sh

This tool generates a script with the needed commands to create/update a funtoo-flavor meta-repo.

> Usage:
>
>   gen-meta-repo.sh [--update-sha1] <release-version> <list of: repo_root[@branch[,branch,...[,*]]]>
>
>       Generate script with commands needed to create/update a funtoo-flavored release meta-repo based on the supplied repos/branches.
>       For each repo_root specified, one or more branches (comma separated) may be specified; they will be applied in order, the first of which will act as the default branch.
>       Specifying no branch will include all branches in the repo. Appending a '*' to the branch list will include all remaining branches in repo following sorted list.
>
>       <release-version>   Version name/number for this release
>       <repo_root>         Path to root of a repo to use in release
>           [@branch,...]   Name of branches in repo to be included in release, all if none specified
>               '*'         Include all remaining branches in repo after listed branches
>       Options:
>           --update-sha1   Only update metadata/kits-sha1.json to reflect latest hashes for branches

Helper used by phkg2-phkr to create meta repo.


## mf4cpv.py

Tool for extracting manifest entries for specified cpv (cat/pkg-ver) from g2-format repos.

> Usage:
>
>   mf4cpv.py <hashtype> <reponame> <reporoot> <category> <package> <version>
>
>       <hashtype>  Hash entry type to extract (SHA1,BLAKE2B,etc.)
>       <reponame>  Name of repository
>       <reporoot>  Path to root of repo
>       <category>  Portage category of package
>       <package>   Name of package in portage
>       <version>   Package version (full)

Note: This doesn't work in many cases due to not finding required eclasses, workaround needed.


## phkg2-phkr.sh - Generate release

This tool calls the rest to do all the heavy lifting. It pulls needed phrags, merges phkits, and generates a meta-repo.

> Usage:
>
>     phkg2-phkr.sh [--up|--deep] <.phkr file>
>
>       Release management tool.
>
>       <.phkr file>    Path to a .phkr file defining a release.
>
>       Options:
>           --deep forces full rebuild distfile pull and digest regen
>           --up

Example:
```
cd /var/git/phkg2/phkrs/phkrs-funtoo
sh /var/git/phkg2/phrackaged2ls/2ls/phkg2-phkr.sh --deep funtoo-stage3-multilib@1.3-release.phkr
```


## phkg2-phrag-getit.sh

This tool pulls/updates selected/all phrags in the project.

> Usage:
>
>       phkg2-phrag-getit.sh <all|list of: phrags>
>
>           Clone/pull phrags from gitlab repo.
>
>           all     git pulls all available phrags in the project
>           <phrag> git pull listed phrags ( w/ 'phrag-' prepended if needed)

Example:
```
cd /var/git/phkg2/phrackaged2ls/2ls
sh phkg2-phrag-getit.sh all
```


## phkg2-phrag-merge.sh

This tool merges source repos into a dest repo for a specified phrag/phkit/version.

> Usage:
>
>       phkg2-phrag-merge.sh <phrag> <phkit> <version>
>
>           Create merged repo for specified phrag, phkit, version.
>           (Set FORCE_EBUILD_DIGEST=1 when calling to run 'ebuild $ebuild digest' for all resulting cat/pkgs.)
>
>           <phrag>     Name of phrag to work with
>           <phkit>     Name of phkit to merge
>           <version>   Version of phkit to merge


## phkg2-phrag.sh

This tool handles all 'phrag' operations

> Usage:
>
>       phkg2-phrag.sh <phrag-command> [args]
>
>           Phrackaged2 phrag tool
>
>           phrag-commands:
>               merge <phrag> <phkit> <version>
>                   Runs phrag-merge tool with specified arguments.
>
>               clone <new-phrag> [src-phrag]
>                   Create <new-phrag> from git clone of [src-phrag].
>                   (src-phrag default is 'phrag-skeleton' if not specified.)
>

Example:
```
cd /var/git/phkg2/phrackaged2ls/2ls
sh /var/git/phkg2/phrackaged2ls/2ls/phkg2-phrag.sh merge funtoo funtoo-stage3 palica-master
```
