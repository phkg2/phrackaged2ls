# Phrackaged2
## A Meta-meta-distribution-engine.

**Reporef**: A notation to specify a particular repo, relative path, and state by it's local name, subdirectory with respect to repo root, and brach, tag, or other committish.

```
reporef =: <repo-name>[/<subdir>][@<gitref>]
```

**Phrag**: Broad grouping of related phkits maintained together.

```
phrag-root =: phrag-<phrag-name>
```
 - `<phrag-root>/src_repo.list` - List of source repos and their URIs with format: `^<repo-name>[[:space:]]+<repo-uri>`
 - `<phrag-root>/phrag.fest` - Manifest file for the phrag. (Not yet implemented)
 - `<phrag-root>/phkit.list` - List of phkits available in the phrag. (Not yet implemented)


**Phkit**: A group of closely-related packages with each phkit version specifying explicitly which packages and support files come from which repositories and at what repo state. Additional phkits/subphkits should be created wherever tight inter-package version dependencies exist to avoid version mismatches while maintaining flexability to use different versions of less version-dependent packages.

```
phkit-root =: <phrag-root>/<phkit-name>
```
 - `<phkit-root>/<phkit-name>@<phkit-version>.phkit` - One or more `#REPOREFS=<reporef>...`(space seperated) lines followed by glob patterns determining which specific files are to be included in the destination repository and which source reporefsthey come from. The glob list is reset each time a new `#REPOREFS=` token is encountered.
 - `<phkit-root>/<phkit-name>.phkit_all` - A phkit file which applies to phkits of all versions. (Not yet implemented)
 - `<phkit-root>/<phkit-name>@<phkit-version>.phk_m` - Meta phkit file specify which atoms come from which reporefs (Not yet implemented)
 - `<phkit-root>/<phkit-name>.phk_m_all` - Meta phkit file to specify, for atoms included in all phkit versions, which come from which reporefs. (Not yet implemented)

A `.phkit` file must explicitly include all required files, including eclasses, needed by all included ebuilds. Many of these may be included from the `.phk_m_all` files where they are needed by all versions of a phkit.
(When intra-phkit deps are added, these will need to consider the eclasses available in those parent phkits.)

During merge, a subset of the lines matching only files required by selected ebuilds is extracted from the `Manifest` file in each respective pkg-dir from each src repo using mf4cpv.py.
As the merge progresses, all eclasses selected thus far are collected to a 'global' eclass repo to allow resolution of inherits to work as needed.



**Tools**:
 - phkup -- Repo/global updater (Not yet implemented)
 - phrag-clone -- Clone a new phrag from the skeleton or optionally another existing phrag.
 - phrag-merge -- Takes a phrag, phkit name, and phkit version and performs a merge using a .phkit file and producing a g2-style repo as output.
 - mf4cpv -- Takes: checksum type, repo name, repo root, category, package, version; returns Manifest file entries for all DISTFILES needed by listed cpv.

